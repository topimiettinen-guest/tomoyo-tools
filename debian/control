Source: tomoyo-tools
Section: admin
Priority: optional
Maintainer: Hideki Yamane <henrich@debian.org>
Homepage: https://tomoyo.osdn.jp/
Build-Depends: debhelper (>= 12), debhelper-compat (= 12),
               libncurses5-dev, libreadline-dev, po-debconf
Standards-Version: 4.3.0
Vcs-Git: https://salsa.debian.org/debian/tomoyo-tools.git
Vcs-Browser: https://salsa.debian.org/debian/tomoyo-tools
Rules-Requires-Root: no

Package: tomoyo-tools
Architecture: linux-any
Pre-Depends: debconf
Depends: ${misc:Depends}, ${shlibs:Depends}
Conflicts: tomoyo-ccstools, tomoyo-ccstools1.7
Replaces: tomoyo-ccstools, tomoyo-ccstools1.7
Description: lightweight Linux Mandatory Access Control system
 TOMOYO Linux is a lightweight and easy-to-use path-based Mandatory
 Access Control (MAC) implementation with:
  * automatic policy configuration via "learning" mode;
  * an administrator-friendly policy language;
  * no need for SELinux, or userland program modifications.
 .
 This package provides the audit daemon and administrative utilities for
 use on a Linux kernel with TOMOYO support (standard in Debian kernels).

Package: libtomoyotools3
Section: libs
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: libtomoyotools1, libtomoyotools2
Architecture: linux-any
Recommends: tomoyo-tools
Description: lightweight Linux Mandatory Access Control system - library
 TOMOYO Linux is a lightweight and easy-to-use path-based Mandatory
 Access Control (MAC) implementation with:
  * automatic policy configuration via "learning" mode;
  * an administrator-friendly policy language;
  * no need for SELinux, or userland program modifications.
 .
 This package provides the shared library used by the utilities in the
 tomoyo-tools package.
